# Importing Libraries
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# Importing Dataset
dataset = pd.read_csv("weatherAUS.csv")
# Checking the total Null Values in the Dataset
dataset.isnull().sum()
# Replacing Null Values with 0 in Numerical Columns
dataset[["MinTemp", "MaxTemp", "Rainfall", "Evaporation", "Sunshine", "WindGustSpeed", "WindSpeed9am", "WindSpeed3pm", "Humidity9am", "Humidity3pm", "Pressure9am", "Pressure3pm", "Cloud9am", "Cloud3pm", "Temp9am", "Temp3pm"]] = dataset[["MinTemp", "MaxTemp", "Rainfall", "Evaporation", "Sunshine", "WindGustSpeed", "WindSpeed9am", "WindSpeed3pm", "Humidity9am", "Humidity3pm", "Pressure9am", "Pressure3pm", "Cloud9am", "Cloud3pm", "Temp9am", "Temp3pm"]].fillna(value = 0)
# Removing Rows With Null Value In String Columns For Hot Encoding
dataset = dataset.dropna(axis = 0, subset = ["WindGustDir", "WindDir9am", "WindDir3pm", "RainToday"])
X = dataset.iloc[:, 1:-1].values
y = dataset.iloc[:, 23].values

# Categorical Values
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
labelencoder_Location = LabelEncoder()
X[:, 0] = labelencoder_Location.fit_transform(X[:, 0])
X[:, [6,8,9,20]] = X[:,[6,8,9,20]].astype(str)
labelencoder_WindGustDir = LabelEncoder()
X[:, 6] = labelencoder_WindGustDir.fit_transform(X[:, 6])
labelencoder_WindDir9am = LabelEncoder()
X[:, 8] = labelencoder_WindDir9am.fit_transform(X[:, 8])
labelencoder_WindDir3pm = LabelEncoder()
X[:, 9] = labelencoder_WindDir3pm.fit_transform(X[:, 9])
labelencoder_RainToday = LabelEncoder()
X[:, 20] = labelencoder_RainToday.fit_transform(X[:, 20])
labelencoder_y = LabelEncoder()
y = labelencoder_y.fit_transform(y)

# Splitting Into Train and Test Sets
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X , y, test_size = 0.2, random_state = 0)

# Feature Scaling
from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)

# Selecting the Best SVC Params
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV
parameters = [{'C': [1, 10, 100, 1000], 'kernel': ['linear']},
              {'C': [1, 10, 100, 1000], 'kernel': ['rbf'], 'gamma': [0.5, 0.1, 0.01, 0.001, 0.0001]}]
grid_search = GridSearchCV(estimator = SVC(),
                           param_grid = parameters,
                           scoring = 'accuracy',
                           cv = 10,
                           n_jobs = -1)
grid_search = grid_search.fit(X_train, y_train)
best_accuracy = grid_search.best_score_
best_parameters = grid_search.best_params_


# Fitting Linear SVC To our Dataset
classifier = SVC(C= 100, kernel = 'linear', random_state = 0)
classifier.fit(X_train, y_train)

# Checking the Accuracies
from sklearn.model_selection import cross_val_score
accuracies = cross_val_score(estimator = classifier, X = X_train, y = y_train, cv = 10)
accuracies.mean()
accuracies.std()

# Predicting the Test Sets
y_pred = classifier.predict(X_test)

# Creating the Confusion Matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_pred, y_test)

